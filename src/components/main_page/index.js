import React from "react";

import './index.css';
import Footer from "../common/footer";
import Menubar from "../common/menubar";
import Banner from "./dependencies/banner";
import MainComponent from "./dependencies/MainComponent";
import FourColumn from "./dependencies/four_col";

class Main extends React.Component {

  render() {

    return (
      <>
        <Menubar />
          <Banner />
          <FourColumn/>
          <MainComponent />
        <Footer />
      </>
    )
  }

}

export default Main