const FourColumn = () => {
return(
  <div>
  <section class="one-fourth" id="html">
    <td><i class="fa fa-html5"></i></td>
    <h3>HTML</h3>
  </section>
 <section class="one-fourth" id="css">
 <td><i class="fa fa-css3"></i></td>
   <h3>CSS</h3>
 </section>
 <section class="one-fourth" id="seo">
   <td><i class="fa fa-search"></i></td>
   <h3>SEO</h3>
 </section>
 <section class="one-fourth" id="social">
   <td><i class="fa fa-users"></i></td>
   <h3>Social</h3>
 </section>
 </div>
)
}

export default FourColumn
