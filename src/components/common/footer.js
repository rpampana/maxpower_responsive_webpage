const Footer = () => {
  return (
    <div className="footer">
      <ul className="social">
        <li><a href="#facebook" target="_blank"><i className="fa fa-facebook"></i></a></li>
        <li><a href="#google" target="_blank"><i className="fa fa-google-plus"></i></a></li>
        <li><a href="#twitter" target="_blank"><i className="fa fa-twitter"></i></a></li>
        <li><a href="#youtube" target="_blank"><i className="fa fa-youtube"></i></a></li>
        <li><a href="#instagram" target="_blank"><i className="fa fa-instagram"></i></a></li>
      </ul>
      <footer class="second">
      <p>&copy; MaxPower Design</p>
  </footer>
    </div>
  )
}
export default Footer
